package com.epam.training.anastasiia_darmohrai.optional_task2.page;

import com.epam.training.anastasiia_darmohrai.common.page.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class PastebinResultPage extends AbstractPage {
    @FindBy(xpath = "//h1")
    private WebElement title;

    @FindBy(xpath = "//ol[@class='bash']//li")
    private List<WebElement> displayedCodeLines;

    @FindBy(xpath = "//div[contains(@class, 'bash')]")
    private WebElement bashSyntaxIndicator;

    public PastebinResultPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public PastebinResultPage openPage() {
        return this;
    }

    public String getTitle() {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS));
        wait.until(ExpectedConditions.visibilityOf(title));
        return title.getText();
    }

    public boolean isSyntaxBash() {
        return bashSyntaxIndicator.getAttribute("class").contains("bash");
    }

    public String getDisplayedCode() {
        StringBuilder code = new StringBuilder();
        for (WebElement line : displayedCodeLines) {
            code.append(line.getText()).append("\n");
        }
        return code.toString().trim();
    }
}
