package com.epam.training.anastasiia_darmohrai.final_task.service;

import com.epam.training.anastasiia_darmohrai.final_task.model.LoginPayload;

public class LoginPayloadCreator {
    public static final String TESTDATA_LOGIN_USERNAME_UC1 = "testdata.saucedemo.login.uc1.username";
    public static final String TESTDATA_LOGIN_PASSWORD_UC1 = "testdata.saucedemo.login.uc1.password";
    public static final String TESTDATA_LOGIN_ERROR_UC1 = "testdata.saucedemo.login.uc1.error";

    public static final String TESTDATA_LOGIN_USERNAME_UC2 = "testdata.saucedemo.login.uc2.username";
    public static final String TESTDATA_LOGIN_PASSWORD_UC2 = "testdata.saucedemo.login.uc2.password";
    public static final String TESTDATA_LOGIN_ERROR_UC2 = "testdata.saucedemo.login.uc2.error";

    public static final String TESTDATA_LOGIN_USERNAME_UC3 = "testdata.saucedemo.login.uc3.username";
    public static final String TESTDATA_LOGIN_PASSWORD_UC3 = "testdata.saucedemo.login.uc3.password";
    public static final String TESTDATA_LOGIN_TITLE_UC3 = "testdata.saucedemo.login.uc3.title";

    public static LoginPayload loginPayloadUC1() {
        return new LoginPayload(
                TestDataReader.getTestData(TESTDATA_LOGIN_USERNAME_UC1),
                TestDataReader.getTestData(TESTDATA_LOGIN_PASSWORD_UC1),
                TestDataReader.getTestData(TESTDATA_LOGIN_ERROR_UC1),
                ""
        );
    }

    public static LoginPayload loginPayloadUC2() {
        return new LoginPayload(
                TestDataReader.getTestData(TESTDATA_LOGIN_USERNAME_UC2),
                TestDataReader.getTestData(TESTDATA_LOGIN_PASSWORD_UC2),
                TestDataReader.getTestData(TESTDATA_LOGIN_ERROR_UC2),
                ""
        );
    }

    public static LoginPayload loginPayloadUC3() {
        return new LoginPayload(
                TestDataReader.getTestData(TESTDATA_LOGIN_USERNAME_UC3),
                TestDataReader.getTestData(TESTDATA_LOGIN_PASSWORD_UC3),
                "",
                TestDataReader.getTestData(TESTDATA_LOGIN_TITLE_UC3)
        );
    }
}
