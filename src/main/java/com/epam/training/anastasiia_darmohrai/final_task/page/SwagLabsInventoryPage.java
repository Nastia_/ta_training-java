package com.epam.training.anastasiia_darmohrai.final_task.page;

import com.epam.training.anastasiia_darmohrai.common.page.AbstractPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.training.anastasiia_darmohrai.common.util.WaitUtils.waitForVisibility;

@Slf4j
public class SwagLabsInventoryPage extends AbstractPage {
    @FindBy(xpath = "//div[contains(text(),'Swag Labs')]")
    private WebElement title;

    public SwagLabsInventoryPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public AbstractPage openPage() {
        return this;
    }

    public String getPageTitle() {
        String pageTitle = waitForVisibility(title).getText();
        log.info("Page title: {}", pageTitle);
        return pageTitle;
    }
}
