package com.epam.training.anastasiia_darmohrai.optional_task2.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PasteScript2 {
    private String code;
    private String syntax;
    private String pasteExpiration;
    private String pasteName;
}
