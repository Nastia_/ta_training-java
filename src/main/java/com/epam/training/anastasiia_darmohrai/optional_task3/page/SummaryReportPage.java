package com.epam.training.anastasiia_darmohrai.optional_task3.page;

import com.epam.training.anastasiia_darmohrai.common.page.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.training.anastasiia_darmohrai.common.util.WaitUtils.waitForVisibility;

public class SummaryReportPage extends AbstractPage {
//    @FindBy(xpath = "//span[text()='COMPUTE']/following-sibling::span[contains(text(), '$')]")
    @FindBy(xpath = "//div[span[text()='Compute']]//span[contains(text(), '$')]")
    private WebElement estimatedCost;
    @FindBy(xpath = "//span[text()='Machine type']/following-sibling::span")
    private WebElement machineType;
    @FindBy(xpath = "//span[text()='GPU Model']/following-sibling::span")
    private WebElement gpuModel;
    @FindBy(xpath = "//span[text()='Number of GPUs']/following-sibling::span")
    private WebElement numbersOfGPUs;
    @FindBy(xpath = "//span[text()='Number of Instances']/following-sibling::span")
    private WebElement numberOfInstances;
    @FindBy(xpath = "//span[text()='Operating System / Software']/following-sibling::span")
    private WebElement operatingSystem;
    @FindBy(xpath = "//span[text()='Provisioning Model']/following-sibling::span")
    private WebElement provisioningModel;
    @FindBy(xpath = "//span[text()='Add GPUs']/following-sibling::span")
    private WebElement gpuExisting;
    @FindBy(xpath = "//span[text()='Region']/following-sibling::span")
    private WebElement region;


    public SummaryReportPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected AbstractPage openPage() {
        return this;
    }
    public String getEstimatedCost() {
        return waitForVisibility(estimatedCost).getText();
    }

    public String getNumberOfInstances() {
        return waitForVisibility(numberOfInstances).getText();
    }

    public String getOperatingSystem() {
        return waitForVisibility(operatingSystem).getText();
    }

    public String getProvisioningModel() {
        return waitForVisibility(provisioningModel).getText();
    }

    public String getMachineType() {
        return waitForVisibility(machineType).getText();
    }

    public String getGpuModel() {
        return waitForVisibility(gpuModel).getText();
    }

    public String getNumberOfGPUs() {
        return waitForVisibility(numbersOfGPUs).getText();
    }

    public String getRegion() {
        return waitForVisibility(region).getText();
    }
}
