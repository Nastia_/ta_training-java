# Swag Labs Login Automation

This project contains a final task with a Selenium WebDriver automation suite for testing the login functionality on [Swag Labs](https://www.saucedemo.com/). The suite covers essential test cases for login form validations and is designed for cross-browser execution on Firefox, Edge and Chrome. The project is built with Maven and follows best practices in automation, including logging, parallel execution, and data-driven testing and additionally Behavior-driven approach.

## Project Setup

- **Test Automation Tool**: Selenium WebDriver
- **Project Builder**: Maven
- **Browsers**: Firefox, Edge, Chrome
- **Locators**: XPath
- **Test Runner**: JUnit
- **Assertions**: Hamcrest
- **Logging**: SLF4J
- **Testing Approaches**:
  - **Data-Driven Testing (DDT)**: Parameterized tests to run multiple data sets.
  - **Behavior-Driven Development (BDD)**: Using Cucumber for writing scenarios.



## Task Description

This automation suite includes the following use cases:

### UC-1: Test Login Form with Empty Credentials

1. Input any text into the "Username" and "Password" fields.
2. Clear the inputs.
3. Click the "Login" button.
4. Verify the error message displays: **"Username is required"**.

### UC-2: Test Login Form with Username Only

1. Enter any valid username.
2. Input any text into the "Password" field, then clear it.
3. Click the "Login" button.
4. Verify the error message displays: **"Password is required"**.

### UC-3: Test Login Form with Valid Credentials

1. Enter a username from the accepted users list.
2. Input **"secret_sauce"** as the password.
3. Click the "Login" button.
4. Verify the page title on successful login is **"Swag Labs"**.

## Key Features

- **Parallel Execution**: Tests can run simultaneously to reduce execution time.
- **Data-Driven Testing**: Utilizes JUnit’s DataProvider to input different sets of test data.
- **Detailed Logging**: SLF4J provides log entries to assist with debugging and monitoring.
- _Optional_ **BDD approach** is applied.

## Getting Started

### Prerequisites

- Install Java (version 17)
- Install Maven
- Install compatible versions of Firefox, Edge and Chrome browsers

### Installation

1. Clone the repository:
   ```bash
   https://gitlab.com/Nastia_/ta_training-java.git

2. Navigate to the project directory and install all dependencies:

   `mvn clean install`

3. To execute DDT test run the command:

   `mvn  -Dtest=LoginTest clean test`

3. To execute BDD test run the command:

   `mvn  -Dtest=CucumberSwagLabsRunner clean test`

   _You can also put properties like browser, for example: -Dbrowser=chrome or env(dev,qa), for example: -Denvironment=qa_

