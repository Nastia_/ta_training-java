package com.epam.training.anastasiia_darmohrai.optional_task1.page;

import com.epam.training.anastasiia_darmohrai.common.page.AbstractPage;
import com.epam.training.anastasiia_darmohrai.optional_task1.model.PasteScript1;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.training.anastasiia_darmohrai.common.util.WaitUtils.waitForVisibility;

public class PastebinPage extends AbstractPage {
    private final String BASE_URL = "https://pastebin.com/";
    @FindBy(id = "postform-text")
    private WebElement codeInput;
    @FindBy(id = "select2-postform-expiration-container")
    private WebElement expirationDropdown;
    @FindBy(id = "postform-name")
    private WebElement titleInput;
    @FindBy(xpath = "//button[contains(text(), 'Create New Paste')]")
    private WebElement createPasteButton;

    public PastebinPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public AbstractPage openPage() {
        driver.get(BASE_URL);
        return this;
    }

    public PastebinPage enterCode(String code) {
        waitForVisibility(codeInput).sendKeys(code);
        return this;
    }

    public PastebinPage enterTitle(String title) {
        titleInput.sendKeys(title);
        return this;
    }

    public PastebinPage selectExpiration(String expiration) {
        expirationDropdown.click();
        WebElement option = waitForVisibility(By.xpath("//li[contains(text(), '" + expiration + "')]"));
        option.click();
        return this;
    }

    public PastebinPage createPaste(PasteScript1 paste) {
        enterCode(paste.getCode());
//                .selectExpiration(paste.getExpirationDropdown())
//                .enterTitle(paste.getTitle());
        createPasteButton.click();
        return this;
    }
}
