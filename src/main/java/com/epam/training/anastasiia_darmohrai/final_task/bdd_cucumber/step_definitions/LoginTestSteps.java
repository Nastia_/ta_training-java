package com.epam.training.anastasiia_darmohrai.final_task.bdd_cucumber.step_definitions;

import com.epam.training.anastasiia_darmohrai.common.driver.DriverSingleton;
import com.epam.training.anastasiia_darmohrai.final_task.page.SwagLabsInventoryPage;
import com.epam.training.anastasiia_darmohrai.final_task.page.SwagLabsLoginPage;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Slf4j
public class LoginTestSteps {
    private WebDriver driver;
    private SwagLabsLoginPage loginPage;
    private SwagLabsInventoryPage inventoryPage;

    @Before
    public void setUp() {
        driver = DriverSingleton.getDriver();
    }

    @Given("user navigates to the login page")
    public void user_navigates_to_the_login_page() {
        log.info("Thread ID: {}",
                Thread.currentThread().getId());

        loginPage = new SwagLabsLoginPage(driver);
        loginPage.openPage();
    }

    @When("the user enters {string} in the username field")
    public void the_user_enters_in_the_username_field(String username) {
        loginPage.inputUsername(username);
    }

    @When("the user enters {string} in the password field")
    public void the_user_enters_in_the_password_field(String password) {
        loginPage.inputPassword(password);
    }

    @When("the user clicks the login button")
    public void the_user_clicks_the_login_button() {
        inventoryPage = loginPage.clickLoginButton();
    }

    @Then("the user should see {string}")
    public void the_user_should_see(String expectedMessage) {
        if (expectedMessage.equals("Username is required") || expectedMessage.equals("Password is required")) {
            String actualErrorMessage = loginPage.getErrorMessage();
            assertThat(actualErrorMessage, is("Epic sadface: " + expectedMessage));
        } else {
            String actualTitle = inventoryPage.getPageTitle();
            assertThat(actualTitle, is(expectedMessage));
        }
    }

    @After
    public void stopBrowser() {
        DriverSingleton.closeDriver();
    }
}
