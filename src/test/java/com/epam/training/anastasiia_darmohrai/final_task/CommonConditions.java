package com.epam.training.anastasiia_darmohrai.final_task;

import com.epam.training.anastasiia_darmohrai.common.driver.DriverSingleton;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.WebDriver;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CommonConditions {
    protected WebDriver driver;
    @BeforeEach
    public void setUp()
    {
        driver = DriverSingleton.getDriver();
    }

    @AfterEach
    public void stopBrowser()
    {
        DriverSingleton.closeDriver();
    }
}
