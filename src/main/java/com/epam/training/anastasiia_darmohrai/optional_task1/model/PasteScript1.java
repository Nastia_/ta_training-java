package com.epam.training.anastasiia_darmohrai.optional_task1.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PasteScript1 {
    private String code;
    private String expirationDropdown;
    private String title;
}
