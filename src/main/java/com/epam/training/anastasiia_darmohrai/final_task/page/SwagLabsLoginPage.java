package com.epam.training.anastasiia_darmohrai.final_task.page;

import com.epam.training.anastasiia_darmohrai.common.page.AbstractPage;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.training.anastasiia_darmohrai.common.util.WaitUtils.waitForVisibility;

@Slf4j
public class SwagLabsLoginPage extends AbstractPage {
    private final String BASE_URL = "https://www.saucedemo.com/";

    @FindBy(xpath = "//input[@id='user-name']")
    private WebElement usernameInput;
    @FindBy(xpath = "//input[@id='password']")
    private WebElement passwordInput;
    @FindBy(xpath = "//input[@id='login-button']")
    private WebElement loginButton;
    @FindBy(xpath = "//h3[@data-test='error']")
    private WebElement errorMessage;

    public SwagLabsLoginPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public SwagLabsLoginPage openPage() {
        driver.get(BASE_URL);
        return this;
    }

    public SwagLabsLoginPage inputUsername(String username) {
        clearInputWithActions(usernameInput,username);
        usernameInput.sendKeys(username);
        return this;
    }

    public SwagLabsLoginPage inputPassword(String password) {
        clearInputWithActions(passwordInput,password);
        passwordInput.sendKeys(password);
        return this;
    }

    public SwagLabsInventoryPage clickLoginButton() {
        loginButton.click();
        return new SwagLabsInventoryPage(driver);
    }

    public String getErrorMessage() {
        String errorMsg = waitForVisibility(errorMessage).getText();
        log.info("Error message: {}", errorMsg);
        return errorMsg;
    }

    public void clearInputWithActions(WebElement target, String inputName) {
        target.clear();
        log.info("{} input cleared", inputName);
    }
}
