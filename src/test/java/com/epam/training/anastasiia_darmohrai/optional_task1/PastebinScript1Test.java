package com.epam.training.anastasiia_darmohrai.optional_task1;

import com.epam.training.anastasiia_darmohrai.final_task.CommonConditions;
import com.epam.training.anastasiia_darmohrai.optional_task1.model.PasteScript1;
import com.epam.training.anastasiia_darmohrai.optional_task1.page.PastebinPage;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;

import java.util.Objects;

@Slf4j
@Execution(ExecutionMode.CONCURRENT)
@TestMethodOrder(MethodOrderer.Random.class)
public class PastebinScript1Test extends CommonConditions {
    private static final String CODE_TEXT = "git config --global user.name  \"New Sheriff in Town\"\n" +
            "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "git push origin master --force";
    private static final String PASTE_TITLE = "how to gain dominance among developers";
    private static final String EXPIRATION_TIME = "10 Minutes";

    @RepeatedTest(5)
    public void testCreateNewPaste() {
        PastebinPage pastebinPage = new PastebinPage(driver);
        pastebinPage.openPage();
        PasteScript1 newPaste = new PasteScript1(CODE_TEXT, EXPIRATION_TIME,PASTE_TITLE);
        pastebinPage.createPaste(newPaste);

        Assertions.assertTrue(Objects.requireNonNull(driver.getCurrentUrl()).contains("pastebin.com"), "URL does not contain 'pastebin.com'");
    }
}
