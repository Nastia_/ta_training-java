package com.epam.training.anastasiia_darmohrai.common.driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

@Slf4j
public class DriverSingleton {
    private static final ThreadLocal<WebDriver> threadLocalDriver = new ThreadLocal<>();
    private static final Object lock = new Object();

    private DriverSingleton() {
    }

    public static WebDriver getDriver() {
        if (threadLocalDriver.get() == null) {
            synchronized (lock) {
                if (threadLocalDriver.get() == null) {
                    WebDriver driver = createDriver();
                    threadLocalDriver.set(driver);
                    log.info("Driver created: {}", driver);
                }
            }
        }
        return threadLocalDriver.get();
    }

    private static WebDriver createDriver() {
        String browser = System.getProperty("browser", "edge").toLowerCase();
        WebDriver driver;

        try {
            switch (browser) {
                case "firefox":
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                    break;
                case "edge":
                    WebDriverManager.edgedriver().setup();
                    driver = new EdgeDriver();
                    break;
                case "chrome":
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver();
                    break;
                default:
                    throw new IllegalStateException("Unsupported browser: " + browser);
            }

            driver.manage().window().maximize();
            return driver;
        } catch (Exception e) {
            throw new RuntimeException("Failed to initialize WebDriver", e);
        }
    }

    public static void closeDriver() {
        WebDriver driver = threadLocalDriver.get();
        if (driver != null) {
            try {
                log.info("Closing driver: {}", driver);
                driver.quit();
            } finally {
                threadLocalDriver.remove();
            }
        }
    }
}
