package com.epam.training.anastasiia_darmohrai.final_task.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginPayload {
    private String username;
    private String password;
    private String errorMessage;
    private String title;
}
