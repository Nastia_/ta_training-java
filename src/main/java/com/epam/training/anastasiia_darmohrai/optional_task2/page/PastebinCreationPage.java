package com.epam.training.anastasiia_darmohrai.optional_task2.page;

import com.epam.training.anastasiia_darmohrai.common.page.AbstractPage;
import com.epam.training.anastasiia_darmohrai.optional_task2.model.PasteScript2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PastebinCreationPage extends AbstractPage{
    private final String BASE_URL = "https://pastebin.com/";
    @FindBy(id = "postform-text")
    private WebElement codeInput;

    @FindBy(id = "select2-postform-format-container")
    private WebElement syntaxHighlightingDropdown;

    @FindBy(id = "select2-postform-expiration-container")
    private WebElement expirationDropdown;

    @FindBy(id = "postform-name")
    private WebElement titleInput;

    @FindBy(xpath = "//button[contains(text(), 'Create New Paste')]")
    private WebElement createPasteButton;

    public PastebinCreationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public AbstractPage openPage() {
        driver.get(BASE_URL);
        return this;
    }

    public PastebinCreationPage enterCode(String code) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS));
        wait.until(ExpectedConditions.visibilityOf(codeInput)).sendKeys(code);
        return this;
    }

    public PastebinCreationPage enterTitle(String title) {
        titleInput.sendKeys(title);
        return this;
    }

    public PastebinCreationPage selectSyntaxHighlighting(String syntaxHighlighting) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS));
        syntaxHighlightingDropdown.click();

        WebElement option = wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//li[contains(text(), '" + syntaxHighlighting + "')]")));
        option.click();
        return this;
    }

    public PastebinCreationPage selectExpiration(String expiration) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_SECONDS));
        expirationDropdown.click();

        WebElement option = wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//li[contains(text(), '" + expiration + "')]")));
        option.click();
        return this;
    }

    public PastebinResultPage createPaste(PasteScript2 pasteScript2) {
        enterCode(pasteScript2.getCode())
                .selectSyntaxHighlighting(pasteScript2.getSyntax())
                .selectExpiration(pasteScript2.getPasteExpiration())
                .enterTitle(pasteScript2.getPasteName());
        createPasteButton.click();
        return new PastebinResultPage(driver);
    }
}
