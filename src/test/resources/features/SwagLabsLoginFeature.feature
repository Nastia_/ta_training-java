Feature: Login to Swag Labs website (https://www.saucedemo.com/)

  @login
  Scenario Outline: Test the  the Login form with a variety of credential combinations.
    Given user navigates to the login page
    When the user enters "<username>" in the username field
    * the user enters "<password>" in the password field
    * the user clicks the login button
    Then the user should see "<expectedMessage>"

    Examples:
      | username                | password     | expectedMessage      |
      |                         |              | Username is required |
      | visual_user             |              | Password is required |
      | standard_user           | secret_sauce | Swag Labs            |
      | locked_out_user         | secret_sauce | Swag Labs            |
      | performance_glitch_user | secret_sauce | Swag Labs            |
      | error_user              | secret_sauce | Swag Labs            |