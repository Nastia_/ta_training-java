package com.epam.training.anastasiia_darmohrai.optional_task3;

import com.epam.training.anastasiia_darmohrai.final_task.CommonConditions;
import com.epam.training.anastasiia_darmohrai.optional_task3.model.ComputerEngineModel;
import com.epam.training.anastasiia_darmohrai.optional_task3.page.ComputerEngineFormPage;
import com.epam.training.anastasiia_darmohrai.optional_task3.page.GoogleCloudPricingCalculatorMainPage;
import com.epam.training.anastasiia_darmohrai.optional_task3.page.SummaryReportPage;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class GoogleCloudPricingCalculatorTest extends CommonConditions {

    @Test
    public void testGoogleCloudComputerEngineEstimator() {
        ComputerEngineModel computerEngineModel = createComputerEngineModel();
        String expectedCost = new GoogleCloudPricingCalculatorMainPage(driver)
                .openPage()
                .clickAddToEstimate()
                .fillInForm(computerEngineModel)
                .getEstimatedCost();

        String actualCost = new ComputerEngineFormPage(driver)
                .clickShareButton()
                .openEsteemedSummaryLink()
                .getEstimatedCost();

        var estimateSummary = new SummaryReportPage(driver);

        String actualNumberOfInstances = computerEngineModel.getNumberOfInstances();
        String actualOperatingSystem = computerEngineModel.getOperatingSystem();
        String actualProvisioningModel = computerEngineModel.getProvisioningModel();
        String actualMachineType = computerEngineModel.getMachineType();
        String actualGpuModel = computerEngineModel.getGpuType();
        String actualNumberOfGPUs = computerEngineModel.getNumberOfGpus();
        String actualLocalSSD = computerEngineModel.getLocalSSD();
        String actualRegion = computerEngineModel.getRegion();

        assertEquals(expectedCost, actualCost, "Costs does not match.");
        assertThat(estimateSummary.getNumberOfInstances(), is(actualNumberOfInstances));
        assertThat(estimateSummary.getOperatingSystem(), is(actualOperatingSystem));
        assertThat(estimateSummary.getProvisioningModel(), is(actualProvisioningModel));
        assertThat(estimateSummary.getMachineType(), containsString(actualMachineType));
        assertThat(estimateSummary.getGpuModel(), matchesPattern(".*NVIDIA V100.*"));
        assertThat(estimateSummary.getNumberOfGPUs(), containsString(actualNumberOfGPUs));

        String expectedRegion = "Netherlands (" + actualRegion + ")";
        assertThat(expectedRegion, containsString(estimateSummary.getRegion()));

    }


    private ComputerEngineModel createComputerEngineModel() {
        return ComputerEngineModel.builder()
                .numberOfInstances("4")
                .operatingSystem("Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)")
                .provisioningModel("Regular")
                .machineFamily("General purpose")
                .series("n1")
                .machineType("n1-standard-8")
                .addGpus(true)
                .gpuType("NVIDIA TESLA V100")
                .numberOfGpus("1")
                .region("europe-west4")
                .build();
    }
}
