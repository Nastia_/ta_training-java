package com.epam.training.anastasiia_darmohrai.optional_task3.page;

import com.epam.training.anastasiia_darmohrai.common.page.AbstractPage;
import com.epam.training.anastasiia_darmohrai.optional_task3.model.ComputerEngineModel;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.training.anastasiia_darmohrai.common.util.WaitUtils.waitForElementToBeClickable;
import static com.epam.training.anastasiia_darmohrai.common.util.WaitUtils.waitForVisibility;

public class ComputerEngineFormPage extends AbstractPage {
    private final String SELECT_OPTION_LOCATOR = "//li[@data-value='%s']";
    private final String SELECT_PROVISIONING_MODEL_LOCATOR = "//label[text()='%s']";

    @FindBy(xpath = "//input[@value='1']")
    private WebElement numberOfInstances;
    @FindBy(xpath = "//ul[@aria-label='Operating System / Software']/../..")
    private WebElement operatingSystemSelectDropdown;

    @FindBy(xpath = "//ul[@aria-label='Machine Family']/../..")
    private WebElement machineFamilySelectDropdown;
    @FindBy(xpath = "//ul[@aria-label='Series']/../..")
    private WebElement seriesSelectDropdown;
    @FindBy(xpath = "//ul[@aria-label='Machine type']/../..")
    private WebElement machineTypeSelectDropdown;
    @FindBy(xpath = "//button[@role='switch' and @aria-label='Add GPUs']")
    private WebElement gpuToggleButton;
    @FindBy(xpath = "//ul[@aria-label='GPU Model']/../..")
    private WebElement gpuModelSelectDropdown;
    @FindBy(xpath = "//ul[@aria-label='Number of GPUs']/../..")
    private WebElement numbersOfGpuSelectDropdown;
    @FindBy(xpath = "//ul[@aria-label='Region']/../..")
    private WebElement regionSelectDropdown;
    @FindBy(xpath = "//span[text()='Share']")
    private WebElement shareButton;
    @FindBy(xpath = "//a[contains(text(), 'Open estimate summary')]")
    private WebElement esteemedSummaryLink;
    @FindBy(xpath = "//div[text()='Estimated cost']/following::label[contains(text(), '$')]")
    private WebElement estimatedCost;

    public ComputerEngineFormPage(WebDriver driver) {
        super(driver);
    }

    @Override
    protected AbstractPage openPage() {
        return this;
    }

    public ComputerEngineFormPage fillInForm(ComputerEngineModel computerEngineModel) {
        setNumberOfInstances(computerEngineModel.getNumberOfInstances())

                .selectOperatingSystemOption(computerEngineModel.getOperatingSystem())

                .selectProvisioningModel(computerEngineModel.getProvisioningModel())

                .selectNSeriesOption(computerEngineModel.getSeries())

                .selectMachineFamilyOption(computerEngineModel.getMachineFamily())

                .selectMachineTypeOption(computerEngineModel.getMachineType())

                .toggleGpu(computerEngineModel.isAddGpus())

                .selectGpuModelOption(computerEngineModel.getGpuType())

                .selectNumbersOfGpuOption(computerEngineModel.getNumberOfGpus())

                .selectRegionOption(computerEngineModel.getRegion());
        return this;
    }


    public ComputerEngineFormPage setNumberOfInstances(String number) {
        waitForVisibility(numberOfInstances).clear();
        numberOfInstances.sendKeys(number);

        return this;
    }

    public ComputerEngineFormPage selectOperatingSystemOption(String operatingSystem) {
        return selectOption(operatingSystemSelectDropdown, operatingSystem);
    }

    public ComputerEngineFormPage selectProvisioningModel(String model) {
        String provisionLocator = String.format(SELECT_PROVISIONING_MODEL_LOCATOR, model);
        WebElement element = waitForElementToBeClickable(By.xpath(provisionLocator));
        jsClick(element);

        return this;
    }

    public ComputerEngineFormPage selectMachineFamilyOption(String machineFamilyOption) {
        return selectOption(machineFamilySelectDropdown, machineFamilyOption);
    }

    public ComputerEngineFormPage selectNSeriesOption(String nSeriesOption) {
        return selectOption(seriesSelectDropdown, nSeriesOption);
    }

    public ComputerEngineFormPage selectMachineTypeOption(String machineTypeOption) {
        return selectOption(machineTypeSelectDropdown, machineTypeOption);
    }

    public ComputerEngineFormPage toggleGpu(boolean enable) {
        waitForVisibility(gpuToggleButton);

        boolean isChecked = gpuToggleButton.getAttribute("aria-checked").equals("true");

        if (isChecked != enable) {
            gpuToggleButton.click();
        }
        return this;
    }

    public ComputerEngineFormPage selectGpuModelOption(String gpuModelOption) {
        waitForVisibility(gpuModelSelectDropdown);
        return selectOption(gpuModelSelectDropdown, gpuModelOption);
    }

    public ComputerEngineFormPage selectNumbersOfGpuOption(String numbers) {
        return selectOption(numbersOfGpuSelectDropdown, numbers);
    }

    public ComputerEngineFormPage selectRegionOption(String region) {
        waitForVisibility(regionSelectDropdown);
        return selectOption(regionSelectDropdown, region);
    }

    public ComputerEngineFormPage clickShareButton() {
        shareButton.click();
        return this;
    }

    public SummaryReportPage openEsteemedSummaryLink() {
        String currentTab = driver.getWindowHandle();

        waitForVisibility(esteemedSummaryLink).click();
        for (String tab : driver.getWindowHandles()) {
            if (!tab.equals(currentTab)) {
                driver.switchTo().window(tab);
                break;
            }
        }
        return new SummaryReportPage(driver);
    }

    public String getEstimatedCost() {
        waitForVisibility(estimatedCost);
        return estimatedCost.getText();
    }

    private ComputerEngineFormPage selectOption(WebElement selectBox, String option) {
        selectBox.click();
        String optionXPath = String.format(SELECT_OPTION_LOCATOR, transformStringToDataValue(option));
        waitForElementToBeClickable(By.xpath(optionXPath)).click();
        return this;
    }

    private String transformStringToDataValue(String input) {
        return input.toLowerCase()
                .replaceAll("[,:;()]", "")
                .replaceAll("\\s+", "-");
    }

    public void jsClick(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", element);
    }
}
