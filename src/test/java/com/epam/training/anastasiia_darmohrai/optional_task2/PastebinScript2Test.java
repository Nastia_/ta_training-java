package com.epam.training.anastasiia_darmohrai.optional_task2;

import com.epam.training.anastasiia_darmohrai.final_task.CommonConditions;
import com.epam.training.anastasiia_darmohrai.optional_task2.model.PasteScript2;
import com.epam.training.anastasiia_darmohrai.optional_task2.page.PastebinCreationPage;
import com.epam.training.anastasiia_darmohrai.optional_task2.page.PastebinResultPage;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class PastebinScript2Test extends CommonConditions  {
    private static final String CODE_TEXT = "git config --global user.name  \"New Sheriff in Town\"\n" +
            "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
            "git push origin master --force";
    private static final String PASTE_TITLE = "how to gain dominance among developers";
    private static final String SYNTAX_HIGHLIGHTING = "Bash";
    private static final String EXPIRATION_TIME = "10 Minutes";

    @Test
    public void testCreateNewPaste() {
        PastebinCreationPage pastebinCreationPage = new PastebinCreationPage(driver);
        pastebinCreationPage.openPage();
        PasteScript2 newPaste = new PasteScript2(CODE_TEXT, SYNTAX_HIGHLIGHTING, EXPIRATION_TIME, PASTE_TITLE);
        PastebinResultPage pastebinResultPage = pastebinCreationPage.createPaste(newPaste);

        assertEquals(PASTE_TITLE, pastebinResultPage.getTitle(), "Paste title does not match.");
        assertEquals(CODE_TEXT, pastebinResultPage.getDisplayedCode(), "Displayed code does not match.");
        assertTrue(pastebinResultPage.isSyntaxBash(), "Syntax highlighting is not Bash.");
    }
}
