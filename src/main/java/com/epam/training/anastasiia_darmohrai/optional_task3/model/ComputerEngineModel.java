package com.epam.training.anastasiia_darmohrai.optional_task3.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ComputerEngineModel {
    private String numberOfInstances;
    private String operatingSystem;
    private String provisioningModel;
    private String machineFamily;
    private String series;
    private String machineType;
    private boolean addGpus;
    private String gpuType;
    private String numberOfGpus;
    private String localSSD;
    private String region;
}
