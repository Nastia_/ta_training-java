package com.epam.training.anastasiia_darmohrai.final_task.service;

import lombok.extern.slf4j.Slf4j;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

@Slf4j
public class TestDataReader {

    private static final String ENV_PROPERTY = "env";
    private static final String DEFAULT_ENV = "qa";
    private static final ResourceBundle resourceBundle;

    static {
        String env = System.getProperty(ENV_PROPERTY, DEFAULT_ENV);
        try {
            resourceBundle = ResourceBundle.getBundle(env);
        } catch (MissingResourceException e) {
            throw new RuntimeException("Resource bundle not found for environment: " + env, e);
        }
        log.info("Environment is set to {}", env);
    }

    public static String getTestData(String key) {
        try {
            return resourceBundle.getString(key);
        } catch (MissingResourceException e) {
            throw new RuntimeException("Key not found in resource bundle: " + key, e);
        }
    }
}