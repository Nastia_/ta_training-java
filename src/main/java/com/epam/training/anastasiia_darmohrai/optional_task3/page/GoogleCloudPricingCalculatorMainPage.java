package com.epam.training.anastasiia_darmohrai.optional_task3.page;

import com.epam.training.anastasiia_darmohrai.common.page.AbstractPage;
import com.epam.training.anastasiia_darmohrai.final_task.page.SwagLabsLoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.training.anastasiia_darmohrai.common.util.WaitUtils.waitForVisibility;

public class GoogleCloudPricingCalculatorMainPage extends AbstractPage {
    private final String BASE_URL = "https://cloud.google.com/products/calculator";
    @FindBy(xpath = "//span[text() = 'Add to estimate']")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//div[@class='aHij0b-aGsRMb']")
    private WebElement computeEngineSelect;

    public GoogleCloudPricingCalculatorMainPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public GoogleCloudPricingCalculatorMainPage openPage() {
        driver.get(BASE_URL);
        return this;
    }

    public ComputerEngineFormPage clickAddToEstimate() {
        addToEstimateButton.click();
        waitForVisibility(computeEngineSelect).click();

        return new ComputerEngineFormPage(driver);
    }
}
