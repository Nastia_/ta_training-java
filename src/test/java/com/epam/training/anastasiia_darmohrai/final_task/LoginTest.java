package com.epam.training.anastasiia_darmohrai.final_task;

import com.epam.training.anastasiia_darmohrai.final_task.model.LoginPayload;
import com.epam.training.anastasiia_darmohrai.final_task.page.SwagLabsLoginPage;
import com.epam.training.anastasiia_darmohrai.final_task.service.LoginPayloadCreator;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.parallel.Execution;
import org.junit.jupiter.api.parallel.ExecutionMode;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

@Slf4j
@Execution(ExecutionMode.CONCURRENT)
@TestMethodOrder(MethodOrderer.Random.class)
public class LoginTest extends CommonConditions {

    @ParameterizedTest
    @MethodSource("loginPayloadProvider")
    public void testLoginForm(LoginPayload loginPayload) {
        log.info("Thread ID: {} - Login test with username: {}, password: {}",
                Thread.currentThread().getId(),
                loginPayload.getUsername(),
                loginPayload.getPassword());

        SwagLabsLoginPage loginPage = new SwagLabsLoginPage(driver)
                .openPage()
                .inputUsername(loginPayload.getUsername())
                .inputPassword(loginPayload.getPassword());

        if (loginPayload.getErrorMessage() != null && !loginPayload.getErrorMessage().isEmpty()) {
            loginPage.clickLoginButton();
            String actualErrorMessage = loginPage.getErrorMessage();

            assertThat(actualErrorMessage, containsString(loginPayload.getErrorMessage()));
        } else {
            String actualTitle = loginPage.clickLoginButton().getPageTitle();

            assertThat(actualTitle, is(loginPayload.getTitle()));
        }
        log.info("Login test has successfully passed");
    }

    static Stream<Arguments> loginPayloadProvider() {
        return Stream.of(
                Arguments.of(LoginPayloadCreator.loginPayloadUC1()),
                Arguments.of(LoginPayloadCreator.loginPayloadUC2()),
                Arguments.of(LoginPayloadCreator.loginPayloadUC3())
        );
    }
}
